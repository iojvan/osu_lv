import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, AgglomerativeClustering


def generate_data(n_samples, flagc):
    # 3 grupe
    if flagc == 1:
        random_state = 365
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
    
    # 3 grupe
    elif flagc == 2:
        random_state = 148
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    # 4 grupe 
    elif flagc == 3:
        random_state = 148
        X, y = make_blobs(n_samples=n_samples,
                        centers = 4,
                        cluster_std=np.array([1.0, 2.5, 0.5, 3.0]),
                        random_state=random_state)
    # 2 grupe
    elif flagc == 4:
        X, y = make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
    # 2 grupe  
    elif flagc == 5:
        X, y = make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

# generiranje podatkovnih primjera
X = generate_data(500, 1)

# prikazi primjere u obliku dijagrama rasprsenja
plt.figure()
plt.scatter(X[:,0],X[:,1])
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri')
plt.show()


# 1. Pokrenite skriptu. Prepoznajete li koliko ima grupa u generiranim podacima? Mijenjajte
# nacin generiranja podataka.

fig, ax = plt.subplots(2,2,  figsize=(10,10))
fig.subplots_adjust(hspace=0.3, wspace=0.3)

flags = [2,3,4,5]
samples = 500

i = 0
for flagc in flags:
    X = generate_data(samples, flagc)
    ax[i//2, i%2].scatter(X[:,0], X[:,1])
    ax[i//2, i%2].set_xlabel('$x_1$')
    ax[i//2, i%2].set_ylabel('$x_2$')
    ax[i//2, i%2].set_title(f'Samples={samples}, flagc={flagc}')
    i+=1

plt.show()

# 2. Primijenite metodu K srednjih vrijednosti te ponovo prikažite primjere, ali svaki primjer
# obojite ovisno o njegovoj pripadnosti pojedinoj grupi. Nekoliko puta pokrenite programski
# kod. Mijenjate broj K. Što primjecujete?

fig, ax = plt.subplots(2,2,  figsize=(10,10))
fig.subplots_adjust(hspace=0.3, wspace=0.3)

flags = [2,3,4,5]
groups = [3,4,2,2]
samples = 500

color_theme = np.array(['darkgray', 'lightsalmon', 'powderblue', 'lightgreen'])

i = 0
for flagc in flags:
    X = generate_data(samples, flagc)

    km = KMeans(n_clusters=groups[i], init='random', n_init=5, random_state=0)
    km.fit(X)

    labels = km.labels_

    for j in range(groups[i]):
        ax[i//2, i%2].scatter(X[labels==j,0], X[labels==j,1], c=color_theme[j], label=f'Klasa {j+1}')

    ax[i//2, i%2].set_xlabel('$x_1$')
    ax[i//2, i%2].set_ylabel('$x_2$')
    ax[i//2, i%2].set_title(f'Samples={samples}, flagc={flagc}')
    ax[i//2, i%2].legend(loc='upper right')
    i+=1
    
plt.show()

# 3. Mijenjajte nacin definiranja umjetnih primjera te promatrajte rezultate grupiranja podataka
# (koristite optimalni broj grupa). Kako komentirate dobivene rezultate?
