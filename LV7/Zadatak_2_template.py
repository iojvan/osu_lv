import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

# 1. Otvorite skriptu zadatak_2.py . Ova skripta ucitava originalnu RGB sliku test_1.jpg
# te ju transformira u podatkovni skup koji dimenzijama odgovara izrazu 7.2 pri cemu je n
# broj elemenata slike, a m je jednak 3. Koliko je razlicitih boja prisutno u ovoj slici?

unique_colors = np.unique(img_array_aprox, axis=0).shape[0]
print(f'Broj boja: {unique_colors}')

# 2. Primijenite algoritam K srednjih vrijednosti koji ce pronaci grupe u RGB vrijednostima
# elemenata originalne slike.

color_table = img.reshape(-1,3)
print(pd.DataFrame(color_table,columns=['Red','Green','Blue']))

km = KMeans(n_clusters=5)
km.fit(color_table)

converted_img = km.cluster_centers_[km.labels_].reshape(img.shape)

fig, ax = plt.subplots(2,1, figsize=(7,7))

ax[0].imshow(img)
ax[0].set_title(f'Broj boja: {unique_colors}')

ax[1].imshow(converted_img)
ax[1].set_title(f'Broj boja: {km.cluster_centers_.shape[0]}')

plt.show()


IMG_FOLDER = 'imgs\\'
images = ['test_2.jpg','test_3.jpg','test_5.jpg','test_6.jpg']

# for image in images:
#     img = Image.imread(IMG_FOLDER+image)
#     img = img.astype(np.float64) / 255

#     color_table = img.reshape(-1,img.shape[2])

#     km = KMeans(n_clusters=5)
#     km.fit(color_table)

#     converted_img = km.cluster_centers_[km.labels_].reshape(img.shape)

#     fig, ax = plt.subplots(2,1, figsize=(7,7))

#     ax[0].imshow(img)
#     unique_colors = np.unique(img, axis=0).shape[0]
#     ax[0].set_title(f'Broj boja: {unique_colors}')

#     ax[1].imshow(converted_img)
#     ax[1].set_title(f'Broj boja: {km.cluster_centers_.shape[0]}')

#     plt.show()


# Graficki prikažite ovisnost J o broju grupa K. Koristite atribut inertia objekta klase
# KMeans. Možete li uociti lakat koji upucuje na optimalni broj grupa?

img = Image.imread("imgs\\test_1.jpg")
img = img.astype(np.float64) / 255

color_table = img.reshape(-1, 3)

distortions = []
K = np.array([1,2,3,4,5,6,7,8,9,10])
for k in K:
    
    km = KMeans(n_clusters=k)
    km.fit(color_table)

    distortions.append(km.inertia_)

plt.figure(figsize=(16,8))
plt.plot(K, distortions, 'bx-')
plt.xlabel('k')
plt.ylabel('Distortion')
plt.title('The Elbow Method showing the optimal k')
plt.show()

# 7. Elemente slike koji pripadaju jednoj grupi prikažite kao zasebnu binarnu sliku. Što
# primjecujete?

