data = open('SMSSpamCollection.txt')

spam_words = 0
spam_counter = 0

ham_words = 0
ham_counter = 0

exclamation_mark_counter = 0

for line in data:
    line = line.rstrip()
    
    if line.startswith("ham"):
        ham_counter+=1
        ham_words += len(line.replace("ham",'').split())
    elif line.startswith("spam"):
        spam_counter+=1
        spam_words += len(line.replace("spam",'').split())
        if line.endswith('!'):
            exclamation_mark_counter+=1

print("Average number of spam words: " + str(spam_words/spam_counter))
print("Average number of ham words: " + str(ham_words/ham_counter))
print("Number of spam sms ending with '!': " + str(exclamation_mark_counter))

