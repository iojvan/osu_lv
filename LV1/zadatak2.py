while True:
    try:
        mark = float(input("Ocjena: "))
        if mark < 0 or mark > 1:
            print("Ocijena van raspona!")
        else:
            if mark >= 0.9:
                print("A")
            elif mark >= 0.8 and mark < 0.9:
                print("B")
            elif mark >= 0.7 and mark < 0.8:
                print("C")
            elif mark >= 0.6 and mark < 0.7:
                print("D")
            else:
                print("F")
    except:
        print("Ocijena nije broj!")
