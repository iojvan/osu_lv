def total_euro(hours,rate):
    return hours*rate

while True:
    try:
        hours = float(input("Radni sati: "))
        while hours <= 0:
            hours = float(input("Radni sati: "))

        rate = float(input("eura/h: "))
        while rate <= 0:
            rate = float(input("eura/h: "))

        print("Ukupno: " + str(total_euro(hours,rate)) + " eura")
    except:
        print("Unesite normalne vrijednosti!")
