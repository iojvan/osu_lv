fhead = open('song.txt')
word_dict = {}
word_list = []

for line in fhead:
    line = line.rstrip()
    line = line.replace(',','')

    words = line.split()
    for word in words:
        if word not in word_list:
            word_list.append(word)
            word_dict.update({word:1})
        else:
            word_dict[word]+=1

counter = 0
for key in word_dict:
    if word_dict[key] == 1:
        counter+=1
        print(key)

print("Broj rijeci koje se pojavljuju jednom: " + str(counter))
    


    