# Osnove strojnog učenja 2022/2023
## Sveučilišni studij računarstvo, smjer Programsko inženjerstvo


# Test and train set

```python
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
```

# Standard Scaler

```python 
from sklearn.preprocessing import StandardScaler

standardScaler = StandardScaler()
X_train_n = standardScaler.fit_transform(X_train)
X_test_n = standardScaler.transform(X_test)

```

# Linear Regression

```python
import sklearn.linear_model as lm

linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)

intercept = linearModel.intercept_
theta = linearModel.coef_

score = linearModel.score(X_train_n, y_train)

```

# Logistic Regression

```python
from sklearn.linear_model import LogisticRegression

LogisticRegression_model = LogisticRegression()
LogisticRegression_model.fit(X_train, y_train)
```

# Confusion Matrix

```python
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

cm = confusion_matrix(y_test, y_test_predictions)
disp = ConfusionMatrixDisplay(cm)
disp.plot()

TP = cm[1][1]
TN = cm[0][0]
FP = cm[1][0]
FN = cm[0][1]

tocnost = (TP+TN)/(TP+TN+FP+FN)

preciznost = TP/(TP+FP)

odziv = TP/(TP+FN)
```

# KNN

```python
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler        # obavezan Standard Scaler
from sklearn.model_selection import GridSearchCV

K_range = list(range(1,31))
param_grid = dict(n_neighbors = K_range)

svm_gscv = GridSearchCV(KNeighborsClassifier(), param_grid, cv=10, scoring='accuracy')
svm_gscv.fit(X_train_n, y_train)

print(svm_gscv.best_params_)
optimal_K = svm_gscv.best_params_['n_neighbors']

optimal_KNN_model = KNeighborsClassifier(n_neighbors = optimal_K)
optimal_KNN_model.fit(X_train_n, y_train)

y_test_pred_optimal = optimal_KNN_model.predict(X_test_n)
```

# SVM

```python
from sklearn import svm
from sklearn.model_selection import GridSearchCV

param_grid = {
    'C': [0.01, 0.1, 1, 10, 100, 1000],
    'gamma': [10, 1, 0.1, 0.01, 0.001, 0.0001],
    'kernel' : ['rbf']
}

svm_gscv = GridSearchCV(svm.SVC(), param_grid, scoring='accuracy')
svm_gscv.fit(X_train_n, y_train)

best_C = svm_gscv.best_params_['C']
best_gamma = svm_gscv.best_params_['gamma']

optimal_SVC = svm.SVC(kernel='rbf', C=best_C, gamma=best_gamma)
optimal_SVC.fit(X_train_n, y_train)
```

# Artifical Neural Network

```python
import tensorflow as tf

model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Input(shape=(,)))
model.add(tf.keras.layers.Dense(N, activation='relu'))
model.add(tf.keras.layers.Dense(M, activation='relu'))
model.add(tf.keras.layers.Dense(X, activation='sigmoid'))

model.summary()

model.compile(loss = "categorical_crossentropy",
              optimizer = "adam",
              metrics = ["accuracy",])

model.fit(X_train,
          y_train,
          batch_size=10,
          epochs=150)

model.save("myModel.h5")

model = tf.keras.models.load_model("myModel.h5")

val_loss, val_acc = model.evaluate(X_test, y_test)

predictions = model.predict([X_test])

# for binary classification
y_pred_labels = np.where(predictions >= 0.5, 1, predictions)
y_pred_labels = np.where(predictions < 0.5, 0, y_pred_labels)
```
