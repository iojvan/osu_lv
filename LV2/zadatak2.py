import csv
import numpy as np
import matplotlib.pyplot as plt

csv_fn = "data.csv"

with open(csv_fn,"r") as infile:
    reader = csv.reader(infile)
    next(reader, None)
    x = list(reader)
    values = np.array(x, np.float32)

# a)
size = len(values)
print("Sample size: " + str(size))

# b)
fig = plt.figure(figsize=(9,9))

ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)
  
weight = np.array(values[:,2], np.float32)
height = np.array(values[:,1], np.float32)

ax1.axis([min(weight), max(weight), min(height), max(height)])
ax1.scatter(weight,height,s=1)
ax1.set_title("Broj uzoraka: " + str(size))
ax1.set_xlabel("Težina [kg]")
ax1.set_ylabel("Visina [cm]")


# c)
every_50th_element = np.array(values[0::50], np.float32)
weight_50 = np.array(every_50th_element[:,2])
height_50 = np.array(every_50th_element[:,1])


ax2.scatter(weight_50,height_50,s=1)
ax2.axis([min(weight_50), max(weight_50), min(height_50), max(height_50)])
ax2.set_title("Broj uzoraka: " + str(len(every_50th_element)))
ax2.set_xlabel("Težina [kg]")
ax2.set_ylabel("Visina [cm]")
plt.show()

# d)
print("Shortest height: " + str(min(height_50)))
print("Tallest height: " + str(max(height_50)))
print("Average height: " + str(np.mean(height_50)))

# e)
ind_male = (values[:,0] == 1)
ind_female = (values[:,0] == 0)

males = np.array(values[ind_male])
females = np.array(values[ind_female])

print("Lowest male height: " + str(min(males[0:,1])))
print("Tallest male height: " + str(max(males[0:,1])))
print("Average male height: " + str(np.mean(males[0:,1])))

print()

print("Lowest female height: " + str(min(females[0:,1])))
print("Tallest female height: " + str(max(females[0:,1])))
print("Average female height: " + str(np.mean(females[0:,1])))