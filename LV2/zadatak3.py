import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")
img = img[:,:,0].copy()

fig = plt.figure(figsize=(10,10))
ax1 = fig.add_subplot(3,3,1)
ax1.set_title("Izvorna slika")
ax1.imshow(img, cmap="gray")

ax2 = fig.add_subplot(3,3,2)
ax2.set_title("Posvjetljena slika")
bright_filter = np.array(img)         #svaki element povecan za 8

for i in range(len(img)):
    for j in range(len(img[i])):
        if bright_filter[i][j] + 8 > 255:
            bright_filter[i][j] = 255
        else:
            bright_filter[i][j] += 8
ax2.imshow(bright_filter, cmap="gray")

ax3 = fig.add_subplot(3,3,3)
filter = np.zeros(img.shape)            #sve 0
querter = int((img.shape[1]/4)+1)
half = int(img.shape[1]/2)
filter[:,querter:half] = img[:,querter:half]      #izjednaciti s trazenim rasponom iz slike
ax3.set_title("2 cetvrtina slike")
ax3.imshow(filter, cmap="gray")

ax4 = fig.add_subplot(3,3,4)
copy_img = np.array(img)
copy_img = np.rot90(copy_img, k=-1)    #rotiranje u smjeru kazaljke na satu, k=-1 je u suprotnom smijeru
ax4.set_title("Rotirana slika")
ax4.imshow(copy_img, cmap="gray")

ax5 = fig.add_subplot(3,3,5)
mirror_img = np.array(img)
mirror_img = np.flip(img,0)            #zrcali
ax5.set_title("Zrcalna slika")
ax5.imshow(mirror_img, cmap="gray")

plt.show()
