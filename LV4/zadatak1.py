import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder, StandardScaler
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error, mean_absolute_percentage_error

data = pd.read_csv('data_C02_emission.csv')

#region Columns
MAKE = 'Make'
MODEL = 'Model'
VEHCILE_CLASS = 'Vehicle Class'
ENGINE_SIZE = 'Engine Size (L)'
CYLINDERS = 'Cylinders'
TRANSIMSSION = 'Transmission'
FUEL_TYPE = 'Fuel Type'
FUEL_CONSUMPTION_CITY = 'Fuel Consumption City (L/100km)'
FUEL_CONSUMPTION_HWY = 'Fuel Consumption Hwy (L/100km)'
FUEL_CONSUMPTION_COMB_L = 'Fuel Consumption Comb (L/100km)'
FUEL_CONSUMPTION_COMB_MPG = 'Fuel Consumption Comb (mpg)'
CO2_EMISSIONS = 'CO2 Emissions (g/km)'
#endregion


# Potrebno je izgraditi i vrednovati model koji procjenjuje emisiju C02 plinova na temelju os-
# talih numerickih ulaznih velicina.

# a)
# Odaberite željene numericke velicine specificiranjem liste s nazivima stupaca. Podijelite
# podatke na skup za ucenje i skup za testiranje u omjeru 80%-20%.

X = [ENGINE_SIZE, CYLINDERS, FUEL_CONSUMPTION_CITY, FUEL_CONSUMPTION_HWY, FUEL_CONSUMPTION_COMB_L, FUEL_CONSUMPTION_COMB_MPG]
Y = [CO2_EMISSIONS]


X_train, X_test, y_train, y_test = train_test_split(data[X].to_numpy(),data[Y].to_numpy(),test_size=0.2,random_state=1)


# b)
# Pomo ́cu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova
# o jednoj numerickoj velicini. Pri tome podatke koji pripadaju skupu za ucenje oznacite
# plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom.

plt.scatter(X_train[:,2],y_train,s=7, c='blue')
plt.scatter(X_test[:,2],y_test,s=7,c='red')
plt.xlabel(FUEL_CONSUMPTION_CITY)
plt.ylabel(CO2_EMISSIONS)

plt.legend(labels=['Train data','Test data'])

# c)
# Izvršite standardizaciju ulaznih velicina skupa za ucenje. Prikažite histogram vrijednosti
# jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja
# transformirajte ulazne velicine skupa podataka za testiranje.

standardScaler = StandardScaler()
X_train_n = standardScaler.fit_transform(X_train)
X_test_n = standardScaler.transform(X_test)

fig,ax = plt.subplots(1,2, sharex='col', sharey='row')
fig.subplots_adjust(hspace=0.5,wspace=0.5)

ax[0].hist(X_train[:,2], bins=20)
ax[0].set_title('Izvorni podatci')

ax[1].hist(X_train_n[:,2],bins=10)
ax[1].set_title('Skalirani podatci')

# d)
# Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i
# povežite ih s izrazom 4.6.

linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)

intercept = linearModel.intercept_
print('Intercept: ' + str(intercept))

theta = linearModel.coef_
print('Theta: ' + str(theta))

score = linearModel.score(X_train_n, y_train)
print('Score: %.2f' %score)


# e)
# Izvršite procjenu izlazne velicine na temelju ulaznih velicina skupa za testiranje. Prikažite
# pomo ́cu dijagrama raspršenja odnos izmedu stvarnih vrijednosti izlazne velicine i procjene
# dobivene modelom.

y_pred_train = linearModel.predict(X_test_n)

plt.figure()
plt.scatter(y_test, y_pred_train,s=5)
plt.xlabel('Stvarne CO2 Emisije')
plt.ylabel('Predviđene CO2 Emisije')


# f)
# Izvršite vrednovanje modela na nacin da izracunate vrijednosti regresijskih metrika na
# skupu podataka za testiranje.

acuracy = r2_score(y_test, y_pred_train) * 100
print('Tocnost predviđanja: ' + str(round(acuracy,2)) + "%")

MSE = round(mean_squared_error(y_test, y_pred_train, squared=True),2)
print('MSE: ' + str(MSE))

RMSE = round(mean_squared_error(y_test, y_pred_train, squared=False),2)
print('RMSE: ' + str(RMSE))

MAE = round(mean_absolute_error(y_test, y_pred_train),2)
print('MAE: ' + str(MAE))

MAPE = round(mean_absolute_percentage_error(y_test, y_pred_train),2)
print('MAPE: ' + str(MAPE))

# g)
# Što se dogada s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj
# ulaznih velicina?

size = 0.35

X_train, X_test, y_train, y_test = train_test_split(data[X].to_numpy(),data[Y].to_numpy(),test_size=size,random_state=1)
standardScaler = StandardScaler()
X_train_n = standardScaler.fit_transform(X_train)
X_test_n = standardScaler.fit_transform(X_test)

linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)

y_pred_train = linearModel.predict(X_test_n)

acuracy = r2_score(y_test, y_pred_train) * 100
print('Tocnost predviđanja: ' + str(round(acuracy,2)) + "%")

MSE = round(mean_squared_error(y_test, y_pred_train, squared=True),2)
print('MSE: ' + str(MSE))

RMSE = round(mean_squared_error(y_test, y_pred_train, squared=False),2)
print('RMSE: ' + str(RMSE))

MAE = round(mean_absolute_error(y_test, y_pred_train),2)
print('MAE: ' + str(MAE))

MAPE = round(mean_absolute_percentage_error(y_test, y_pred_train),2)
print('MAPE: ' + str(MAPE))




plt.show()