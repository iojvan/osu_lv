import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder, StandardScaler
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error, mean_absolute_percentage_error

data = pd.read_csv('data_C02_emission.csv')

#region Columns
MAKE = 'Make'
MODEL = 'Model'
VEHCILE_CLASS = 'Vehicle Class'
ENGINE_SIZE = 'Engine Size (L)'
CYLINDERS = 'Cylinders'
TRANSIMSSION = 'Transmission'
FUEL_TYPE = 'Fuel Type'
FUEL_CONSUMPTION_CITY = 'Fuel Consumption City (L/100km)'
FUEL_CONSUMPTION_HWY = 'Fuel Consumption Hwy (L/100km)'
FUEL_CONSUMPTION_COMB_L = 'Fuel Consumption Comb (L/100km)'
FUEL_CONSUMPTION_COMB_MPG = 'Fuel Consumption Comb (mpg)'
CO2_EMISSIONS = 'CO2 Emissions (g/km)'
#endregion

# Na temelju rješenja prethodnog zadatka izradite model koji koristi i kategoricku
# varijable „Fuel Type“ kao ulaznu velicinu. Pri tome koristite 1-od-K kodiranje kategorickih
# velicina. Radi jednostavnosti nemojte skalirati ulazne velicine. Komentirajte dobivene rezultate.
# Kolika je maksimalna pogreška u procjeni emisije C02 plinova u g/km? O kojem se modelu
# vozila radi?

ohe = OneHotEncoder()
encoded_fuel_type = ohe.fit_transform(data[[FUEL_TYPE]]).toarray()

data.drop([[MAKE,MODEL,TRANSIMSSION,VEHCILE_CLASS]])

ct = ColumnTransformer(
    [('Fuel',ohe, [6])], remainder='passthrough'
)
df = ct.fit_transform(data.to_numpy())
#data[FUEL_TYPE] = encoded_fuel_type

print(df[:,6])

X = [FUEL_TYPE,ENGINE_SIZE, CYLINDERS, FUEL_CONSUMPTION_CITY, FUEL_CONSUMPTION_HWY, FUEL_CONSUMPTION_COMB_L, FUEL_CONSUMPTION_COMB_MPG]
Y = [CO2_EMISSIONS]

X_train, X_test, y_train, y_test = train_test_split(data[X].to_numpy(),data[Y].to_numpy(),test_size=0.2,random_state=1)

plt.scatter(X_train[:,2],y_train,s=7, c='blue')
plt.scatter(X_test[:,2],y_test,s=7,c='red')
plt.xlabel(FUEL_CONSUMPTION_CITY)
plt.ylabel(CO2_EMISSIONS)

plt.legend(labels=['Train data','Test data'])

standardScaler = StandardScaler()
X_train_n = standardScaler.fit_transform(X_train)
X_test_n = standardScaler.transform(X_test)

fig,ax = plt.subplots(1,2, sharex='col', sharey='row')
fig.subplots_adjust(hspace=0.5,wspace=0.5)

ax[0].hist(X_train[:,2], bins=20)
ax[0].set_title('Izvorni podatci')

ax[1].hist(X_train_n[:,2],bins=10)
ax[1].set_title('Skalirani podatci')

linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)

intercept = linearModel.intercept_
print('Intercept: ' + str(intercept))

theta = linearModel.coef_
print('Theta: ' + str(theta))

score = linearModel.score(X_train_n, y_train)
print('Score: %.2f' %score)

y_pred_train = linearModel.predict(X_test_n)

plt.figure()
plt.scatter(y_test, y_pred_train,s=5)
plt.xlabel('Stvarne CO2 Emisije')
plt.ylabel('Predviđene CO2 Emisije')

acuracy = r2_score(y_test, y_pred_train) * 100
print('Tocnost predviđanja: ' + str(round(acuracy,2)) + "%")

MSE = round(mean_squared_error(y_test, y_pred_train, squared=True),2)
print('MSE: ' + str(MSE))

RMSE = round(mean_squared_error(y_test, y_pred_train, squared=False),2)
print('RMSE: ' + str(RMSE))

MAE = round(mean_absolute_error(y_test, y_pred_train),2)
print('MAE: ' + str(MAE))

MAPE = round(mean_absolute_percentage_error(y_test, y_pred_train),2)
print('MAPE: ' + str(MAPE))

plt.show()