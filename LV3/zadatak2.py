import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

MAKE = 'Make'
MODEL = 'Model'
VEHCILE_CLASS = 'Vehicle Class'
ENGINE_SIZE = 'Engine Size (L)'
CYLINDERS = 'Cylinders'
TRANSIMSSION = 'Transmission'
FUEL_TYPE = 'Fuel Type'
FUEL_CONSUMPTION_CITY = 'Fuel Consumption City (L/100km)'
FUEL_CONSUMPTION_HWY = 'Fuel Consumption Hwy (L/100km)'
FUEL_CONSUMPTION_COMB_L = 'Fuel Consumption Comb (L/100km)'
FUEL_CONSUMPTION_COMB_MPG = 'Fuel Consumption Comb (mpg)'
CO2_EMISSIONS = 'CO2 Emissions (g/km)'

# a)
# Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.

plt.figure()
data[CO2_EMISSIONS].plot(kind='hist', bins = len(data[CO2_EMISSIONS].unique()))


# b)
# Pomo ́cu dijagrama raspršenja prikažite odnos izmedu gradske potrošnje goriva i emisije
# C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu
# velicina, obojite tockice na dijagramu raspršenja s obzirom na tip goriva.

grouped_by_fuels = data.groupby(FUEL_TYPE)
types = data[FUEL_TYPE].unique()

fig, ax = plt.subplots()
for name,group in grouped_by_fuels:
    ax.scatter(group[FUEL_CONSUMPTION_CITY],group[CO2_EMISSIONS],s=5)
ax.legend(types)

# c)
# Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip
# goriva. Primjecujete li grubu mjernu pogrešku u podacima?

grouped = data.groupby(FUEL_TYPE)
grouped.boxplot(column=FUEL_CONSUMPTION_HWY)

# d)
# Pomocu stupcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu groupby.

grouped_by_feul_type = data.groupby(FUEL_TYPE).agg(Size = (FUEL_TYPE,'size'))
grouped_by_feul_type.plot(kind='bar')

# e)
# Pomocu stupcastog grafa prikažite na istoj slici prosjecnu C02 emisiju vozila s obzirom na
# broj cilindara.

grouped_by_cylinders = data.groupby(CYLINDERS).agg(Average_CO2 = (CO2_EMISSIONS,'mean'))
grouped_by_cylinders.plot(kind='bar')






plt.show()