import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix, ConfusionMatrixDisplay, classification_report

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)



# a)
# Prikažite podatke za ucenje u x1 − x2 ravnini matplotlib biblioteke pri cemu podatke obojite
# s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
# marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
# cmap kojima je moguce definirati boju svake klase.

colors = ['red','blue']

plt.figure(figsize=(10,8))

plt.scatter(X_train[:,0], X_train[:,1], c=y_train, cmap=matplotlib.colors.ListedColormap(colors), s=7 )
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, marker='x', cmap=matplotlib.colors.ListedColormap(colors))

plt.xlabel('X1')
plt.ylabel('X2')

plt.legend(['Class 0','Class 1'])


# b)
# Izgradite model logisticke regresije pomo ́cu scikit-learn biblioteke na temelju skupa poda-
# taka za ucenje.

LogisticRegression_model = LogisticRegression()
LogisticRegression_model.fit(X_train, y_train)

y_train_pred = LogisticRegression_model.predict(X_train)

# c)
# Pronadite u atributima izgradenog modela parametre modela. Prikažite granicu odluke
# naucenog modela u ravnini x1 − x2 zajedno s podacima za ucenje. Napomena: granica
# odluke u ravnini x1 − x2 definirana je kao krivulja: θ0 + θ1x1 + θ2x2 = 0.

intercept = LogisticRegression_model.intercept_.flatten()
print('Intercept: ', intercept)

theta = LogisticRegression_model.coef_.flatten()
print('Theta: ', theta)

decision_boundry = -(intercept+theta[0]*X_train[:,0])/theta[1]

plt.plot(X_train[:,0], decision_boundry, ls='--', color='black')

c = -intercept/theta[1]
m = -theta[0]/theta[1]

print('m: ', m)
print('c: ', c)

xd = [X[:,0].min(), X[:,1].max()]
print(np.array(xd)*m)

yd = m*np.array(xd) + c

plt.fill_between(xd, yd, X_train[:,1].min(), color='tab:blue', alpha=0.2)
plt.fill_between(xd, yd, X_train[:,1].max(), color='tab:orange', alpha=0.2)

# d)
# Provedite klasifikaciju skupa podataka za testiranje pomocu izgradenog modela logisticke
# regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izracunate tocnost,
# preciznost i odziv na skupu podataka za testiranje.

y_test_pred = LogisticRegression_model.predict(X_test)

cm = confusion_matrix(y_test, y_test_pred)
disp = ConfusionMatrixDisplay(cm)
disp.plot()

TP = cm[1][1]
TN = cm[0][0]
FP = cm[1][0]
FN = cm[0][1]

tocnost = (TP+TN)/(TP+TN+FP+FN)
print('Tocnost: %.2f' %tocnost)

preciznost = TP/(TP+FP)
print('Preciznost: %.2f' %preciznost)

odziv = TP/(TP+FN)
print('Odziv: %.2f' %odziv)


# e)
# Prikažite skup za testiranje u ravnini x1 − x2. Zelenom bojom oznacite dobro klasificirane
# primjere dok pogrešno klasificirane primjere oznacite crnom bojom.

plt.figure()
plt.title('Skup za testiranje')
plt.xlabel('X1')
plt.ylabel('X2')

indexiCorrect = []
indexiWrong = []
for i in range(len(y_test)):
    if y_test[i] == y_test_pred[i]:
        indexiCorrect.append(i)
    else:
        indexiWrong.append(i)

correctly_predicted = X_train[indexiCorrect]
wrongly_predicted = X_train[indexiWrong]

plt.scatter(correctly_predicted[:,0], correctly_predicted[:,1], c='green')
plt.scatter(wrongly_predicted[:,0], wrongly_predicted[:,1], c='black')

plt.legend(labels=('Dobro klasificirani','Krivo klasificirani'))



plt.show()